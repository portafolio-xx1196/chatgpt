package main

import (
	"GPTStream/helpers"
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/sashabaranov/go-openai"
)

func main() {
	err := helpers.LoadEnvFile(".env")
	if err != nil {
		return
	}
	client := openai.NewClient(os.Getenv("OPENIA_TOKEN"))
	messages := make([]openai.ChatCompletionMessage, 0)
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Conversation")
	fmt.Println("---------------------")
	messageFinish := ""

	for {
		fmt.Print("-> User: ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)
		messages = append(messages, openai.ChatCompletionMessage{
			Role:    openai.ChatMessageRoleUser,
			Content: text,
		})

		var maxTokens, err = strconv.Atoi(os.Getenv("OPENIA_MAX_TOKENS"))

		if err != nil {
			maxTokens = 200
			return
		}

		req := openai.ChatCompletionRequest{
			Model:     openai.GPT3Dot5Turbo,
			MaxTokens: maxTokens,
			Messages:  messages,
			Stream:    true,
		}

		stream, err := client.CreateChatCompletionStream(
			context.Background(),
			req,
		)

		if err != nil {
			fmt.Printf("ChatCompletionStream error: %v\n", err)
			return
		}

		defer stream.Close()

		fmt.Print("-> Chat GPT: ")

		for {
			response, err := stream.Recv()
			if errors.Is(err, io.EOF) {
				fmt.Println(" ")
				break
			}

			if err != nil {
				fmt.Printf("\nStream error: %v\n", err)
				break
			}

			fmt.Printf(response.Choices[0].Delta.Content)
			messageFinish += response.Choices[0].Delta.Content

		}

		messages = append(messages, openai.ChatCompletionMessage{
			Role:    openai.ChatMessageRoleAssistant,
			Content: messageFinish,
		})
	}
}
