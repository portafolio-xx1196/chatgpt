# Use a lightweight base image
FROM golang:1.19.7-alpine3.17 AS build

# Set environment variables
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Set the working directory
WORKDIR /app

# Build the binary
COPY . ./
RUN go build -ldflags="-w -s" -o /xx1196-gpt

# Use a minimal base image
FROM alpine:3.17
COPY --from=build /xx1196-gpt /xx1196-gpt

# Set the command to run the binary
ENTRYPOINT ["/xx1196-gpt"]
