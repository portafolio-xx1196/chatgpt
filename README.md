# xx1196-gpt

Este es un proyecto que utiliza una imagen Docker para ejecutar una instancia de ChatGPT, un modelo de lenguaje basado en GPT-3.5 de OpenAI.

# Requisitos previos
* Antes de poder ejecutar esta imagen, debes asegurarte de tener instalado Docker en tu sistema. Puedes descargar Docker desde su sitio web oficial: https://www.docker.com/products/docker-desktop
* generar tu propia api key desde openIa y pegarla en la variable de entorno ```OPENIA_TOKEN``` del .env.example
* en la variable de entorno ```OPENIA_MAX_TOKENS``` poner la cantidad de tokens por respuesta, por defecto es 200

# Ejecución
Para ejecutar esta imagen, debes utilizar el siguiente comando:

```bash
docker run --rm -it -v "$(pwd)/.env:/.env" xx1196/go-gpt:latest
```
Este comando utiliza la opción -v para montar el archivo .env en el contenedor. Asegúrate de crear un archivo .env en tu directorio actual, basado en el archivo .env.example proporcionado. También puedes modificar el comando para apuntar al directorio correcto que contiene tu archivo .env.

# Créditos
Este proyecto utiliza el modelo de lenguaje ChatGPT, entrenado por OpenAI. Puedes obtener más información sobre este modelo en https://beta.openai.com/docs/guides/chat.

# Licencia
Este proyecto está disponible bajo la Licencia MIT. Consulta el archivo LICENSE para obtener más información.