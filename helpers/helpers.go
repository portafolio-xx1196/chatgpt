package helpers

import (
	"github.com/joho/godotenv"
)

// LoadEnvFile load .env file
func LoadEnvFile(file string) error {
	err := godotenv.Load(file)
	return err
}
